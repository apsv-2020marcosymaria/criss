package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import model.Publication;
import org.glassfish.jersey.client.ClientConfig;

@WebServlet("/PublicationServlet")
public class PublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = (String) request.getParameter("id");
	
		Client client = ClientBuilder.newClient(new ClientConfig()); 
		Publication publication  = client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/"+id) 
				.request().accept(MediaType.APPLICATION_JSON) 
				.get(Publication.class);
		
		request.setAttribute ("publication", publication);
		getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
