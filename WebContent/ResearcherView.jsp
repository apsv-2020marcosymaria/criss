<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>

<title>Researcher</title>
</head>
<body>

<table>
<tr>
<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th> 
</tr>
        <tr>
           <td> ${ri.id} </td>
           <td>${ri.name}</td>
           <td>${ri.lastname}</td>
           <td><a href="${ri.scopusURL}">${ri.scopusURL}</a></td>
           <td>${ri.email}</td>
        </tr>
</table>

<form action="UpdatePublicationsQueueServlet" method="get">
      <button type="submit">Update publication</button>
</form>

<c:if test="${ri.id == user.id}">
	<form action="CreatePublicationServlet" method="post">
        <input type="text" name="publicationId" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="publicationName" placeholder="Publication Name">
        <input type="text" name="publicationDate" placeholder="Publication Date">
        <input type="text" name="authors" placeholder="Authors">
        <button type="submit">Create publication</button>
	</form>
</c:if>

<table>
<tr>
<th>Publication</th> 
</tr>
<c:forEach items="${publications}" var="pi">
        <tr>
          <td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
        </tr>
</c:forEach>

</table>

</body>
</html>